﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using System.IO;
using Moq;

namespace CharlesTyrwhitt.Mailing.Tests
{
    public abstract class CustomerLeadReaderTests
    {
        protected CustomerLeadXmlReader _reader;
        protected MemoryStream _stream;
        protected IEnumerable<CustomerLead> _customerLeads;

        protected abstract string XmlString { get; }

        [TestFixtureSetUp]
        public void Setup()
        {
            //Arrange
            var validator = new Mock<ICustomerLeadValidator>();
            validator.Setup(x => x.IsValid(It.Is<CustomerLead>(y => string.IsNullOrWhiteSpace(y.Firstname) ||
                                                                    string.IsNullOrWhiteSpace(y.Lastname))))
                    .Returns(false);
            validator.Setup(x => x.IsValid(It.Is<CustomerLead>(y => !string.IsNullOrWhiteSpace(y.Firstname) &&
                                                                    !string.IsNullOrWhiteSpace(y.Lastname))))
                    .Returns(true);


            _reader = new CustomerLeadXmlReader(validator.Object);
            _stream = new MemoryStream(Encoding.UTF8.GetBytes(XmlString));

            //Act
            _customerLeads = _reader.Read(_stream);
        }
    }

    [TestFixture]
    public class WhenReadingAnEmptyCustomerLeadStream : CustomerLeadReaderTests
    {
        protected override string XmlString
        {
            get { return @"<?xml version=""1.0"" encoding=""utf-8""?>"; }
        }

        [Test]
        public void ItReturnsAnEmptyCollection()
        {
            Assert.IsEmpty(_customerLeads);
        }
    }

    [TestFixture]
    public class WhenReadingACustomerLeadStream : CustomerLeadReaderTests
    {
        protected override string XmlString
        {
            get 
            { 
                return @"<?xml version=""1.0"" encoding=""utf-8""?>
                            <CustomerLeads xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" 
                                     xsi:noNamespaceSchemaLocation=""BasicCustomerLead.xsd"">
	                            <CustomerLead>
  		                            <FirstName>Paul</FirstName>
  		                            <LastName>Smith</LastName>
  		                            <Email>psmith@example.com</Email>
	                            </CustomerLead>
	                            <CustomerLead>
  		                            <FirstName>Nicole</FirstName>
  		                            <LastName>Farhi</LastName>
  		                            <Email>nicole.farhi@example.com</Email>
	                            </CustomerLead>
	                            <CustomerLead>
  		                            <FirstName>Raf</FirstName>
  		                            <LastName>Simons</LastName>
  		                            <Email>rafs@example.org</Email>
	                            </CustomerLead>
                            </CustomerLeads>"; 
            }
        }

        [Test]
        public void ItReturnsThreeCustomerLeads()
        {
            Assert.AreEqual(3, _customerLeads.Count());
        }

        [Test]
        public void ItSetsTheFirstName()
        {
            Assert.AreEqual("Paul", _customerLeads.First().Firstname);
        }

        [Test]
        public void ItSetsTheLastName()
        {
            Assert.AreEqual("Smith", _customerLeads.First().Lastname);
        }

        [Test]
        public void ItSetsTheEmail()
        {
            Assert.AreEqual("psmith@example.com", _customerLeads.First().EmailAddress);
        }
    }

    [TestFixture]
    public class WhenReadingAStreamWhichIsNotXml : CustomerLeadReaderTests
    {
        protected override string XmlString
        {
            get
            {
                return @"non valid xml";
            }
        }

        [Test]
        public void ItReturnsNoCustomerLeads()
        {
            Assert.IsEmpty(_customerLeads);
        }
    }

    [TestFixture]
    public class WhenReadingACustomerLeadStreamWithNoFirstName : CustomerLeadReaderTests
    {
        protected override string XmlString
        {
            get
            {
                return @"<?xml version=""1.0"" encoding=""utf-8""?>
                            <CustomerLeads xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" 
                                     xsi:noNamespaceSchemaLocation=""BasicCustomerLead.xsd"">
	                            <CustomerLead>
  		                            <FirstName></FirstName>
  		                            <LastName>Smith</LastName>
  		                            <Email>psmith@example.com</Email>
	                            </CustomerLead>
	                            <CustomerLead>
  		                            <FirstName>Nicole</FirstName>
  		                            <LastName>Farhi</LastName>
  		                            <Email>nicole.farhi@example.com</Email>
	                            </CustomerLead>
                            </CustomerLeads>";
            }
        }

        [Test]
        public void ItReturnsTheValidCustomerLeads()
        {
            Assert.AreEqual("Nicole", _customerLeads.Single().Firstname);
            Assert.AreEqual("Farhi", _customerLeads.Single().Lastname);
            Assert.AreEqual("nicole.farhi@example.com", _customerLeads.Single().EmailAddress);
        }
    }

    [TestFixture]
    public class WhenReadingACustomerLeadStreamWithNoLastName : CustomerLeadReaderTests
    {
        protected override string XmlString
        {
            get
            {
                return @"<?xml version=""1.0"" encoding=""utf-8""?>
                            <CustomerLeads xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" 
                                     xsi:noNamespaceSchemaLocation=""BasicCustomerLead.xsd"">
	                            <CustomerLead>
  		                            <FirstName>Paul</FirstName>
  		                            <LastName></LastName>
  		                            <Email>psmith@example.com</Email>
	                            </CustomerLead>
	                            <CustomerLead>
  		                            <FirstName>Nicole</FirstName>
  		                            <LastName>Farhi</LastName>
  		                            <Email>nicole.farhi@example.com</Email>
	                            </CustomerLead>
                            </CustomerLeads>";
            }
        }

        [Test]
        public void ItReturnsTheValidCustomerLeads()
        {
            Assert.AreEqual("Nicole", _customerLeads.Single().Firstname);
            Assert.AreEqual("Farhi", _customerLeads.Single().Lastname);
            Assert.AreEqual("nicole.farhi@example.com", _customerLeads.Single().EmailAddress);
        }
    }

    [TestFixture]
    public class WhenReadingACustomerLeadStreamWithNoEmail : CustomerLeadReaderTests
    {
        protected override string XmlString
        {
            get
            {
                return @"<?xml version=""1.0"" encoding=""utf-8""?>
                            <CustomerLeads xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" 
                                     xsi:noNamespaceSchemaLocation=""BasicCustomerLead.xsd"">
	                            <CustomerLead>
  		                            <FirstName>Paul</FirstName>
  		                            <LastName>Smith</LastName>
  		                            <Email></Email>
	                            </CustomerLead>
                            </CustomerLeads>";
            }
        }

        [Test]
        public void ItReturnsTheValidCustomerLeads()
        {
            Assert.AreEqual("Paul", _customerLeads.Single().Firstname);
            Assert.AreEqual("Smith", _customerLeads.Single().Lastname);
            Assert.AreEqual("", _customerLeads.Single().EmailAddress);
        }
    }
}
