﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using System.IO;

namespace CharlesTyrwhitt.Mailing.Tests
{
    [TestFixture]
    public class FileLoaderTests
    {
        [Test]
        public void WhenLoadingAFile()
        {
            //Arrange
            string pathToFile = Path.Combine(Environment.CurrentDirectory, "TestFileToLoad.txt");

            //Act
            var stream = new FileLoader().LoadFile(pathToFile);

            //Assert
            using(var streamReader = new StreamReader(stream))
            {
                string fileContent = streamReader.ReadToEnd();
                Assert.AreEqual("Some test text", fileContent);
            }
        }
    }
}
