﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using System.IO;

namespace CharlesTyrwhitt.Mailing.Tests
{
    [TestFixture]
    public class CustomerLeadLoaderIntegrationTests
    {
        CustomerLeadLoader _leadLoader;
        IEnumerable<CustomerLead> _customerLeads;

        [TestFixtureSetUp]
        public void Setup()
        {
            _leadLoader = new CustomerLeadLoader(new FileLoader(), new CustomerLeadXmlReader(new CustomerLeadValidator()));
        }

        [Test]
        public void WhenLoadingAValidCustomerLeadFile()
        {
            //Arrange
            string pathToFile = Path.Combine(Environment.CurrentDirectory, "potentialcustomers.xml");

            //Act
            _customerLeads = _leadLoader.Load(pathToFile);

            //Assert
            Assert.AreEqual(3, _customerLeads.Count());
        }
    }
}
