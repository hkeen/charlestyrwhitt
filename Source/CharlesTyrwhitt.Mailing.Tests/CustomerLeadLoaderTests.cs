﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Moq;
using System.IO;

namespace CharlesTyrwhitt.Mailing.Tests
{
    [TestFixture]
    public class CustomerLeadLoaderTests
    {
        CustomerLeadLoader _loader;
        List<CustomerLead> _customerLeads;
        IEnumerable<CustomerLead> _result;

        [TestFixtureSetUp]
        public void Setup()
        {
            _customerLeads = new List<CustomerLead>();

            var fileLoader = new Mock<IFileLoader>();
            fileLoader.Setup(x => x.LoadFile("some path"))
                .Returns(new MemoryStream());

            var reader = new Mock<ICustomerLeadReader>();
            reader.Setup(x => x.Read(It.IsAny<Stream>()))
                .Returns(_customerLeads);

            _loader = new CustomerLeadLoader(fileLoader.Object, reader.Object);
        }

        [Test]
        public void WhenLoadingCustomerLeadsItLoadsTheFileAndParsesTheCustomerLeadEntities()
        {
            //Act
            _result = _loader.Load("some path");

            //Assert
            Assert.IsNotNull(_result);
            Assert.AreEqual(_customerLeads, _result);
        }
    }

    [TestFixture]
    public class LoadingCustomerLeadsOrderingByLastName
    {
        CustomerLeadLoader _loader;
        List<CustomerLead> _customerLeads;
        IEnumerable<CustomerLead> _result;

        [TestFixtureSetUp]
        public void Setup()
        {
            _customerLeads = new List<CustomerLead>()
                {
                    new CustomerLead(){ Lastname = "Smith" },
                    new CustomerLead(){ Lastname = "Baker" },
                    new CustomerLead(){ Lastname = "Boss" }
                };

            var fileLoader = new Mock<IFileLoader>();
            fileLoader.Setup(x => x.LoadFile("some path"))
                .Returns(new MemoryStream());

            var reader = new Mock<ICustomerLeadReader>();
            reader.Setup(x => x.Read(It.IsAny<Stream>()))
                .Returns(_customerLeads);

            _loader = new CustomerLeadLoader(fileLoader.Object, reader.Object);
        }

        [Test]
        public void WhenLoadingCustomerLeadsItLoadsTheFileAndParsesTheCustomerLeadEntities()
        {
            //Act
            _result = _loader.LoadOrderedByLastName("some path");

            //Assert
            Assert.IsNotNull(_result);
            Assert.AreEqual(_customerLeads.ElementAt(1), _result.ElementAt(0));
            Assert.AreEqual(_customerLeads.ElementAt(2), _result.ElementAt(1));
            Assert.AreEqual(_customerLeads.ElementAt(0), _result.ElementAt(2));
        }
    }
}
