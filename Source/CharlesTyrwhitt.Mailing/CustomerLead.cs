﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CharlesTyrwhitt.Mailing
{
    public class CustomerLead
    {
        public string Firstname { get; set; }

        public string Lastname { get; set; }

        public string EmailAddress { get; set; }
    }
}
