﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CharlesTyrwhitt.Mailing
{
    public class CustomerLeadLoader
    {
        IFileLoader _fileLoader;
        ICustomerLeadReader _reader;

        public CustomerLeadLoader(IFileLoader fileLoader, ICustomerLeadReader reader)
        {
            _fileLoader = fileLoader;
            _reader = reader;
        }

        public IEnumerable<CustomerLead> Load(string filePath)
        {
            var stream = _fileLoader.LoadFile(filePath);
            return _reader.Read(stream);
        }

        public IEnumerable<CustomerLead> LoadOrderedByLastName(string filePath)
        {
            return Load(filePath).OrderBy(x => x.Lastname);
        }
    }
}
