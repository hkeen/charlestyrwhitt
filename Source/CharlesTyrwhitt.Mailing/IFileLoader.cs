﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace CharlesTyrwhitt.Mailing
{
    public interface IFileLoader
    {
        Stream LoadFile(string path);
    }
}
