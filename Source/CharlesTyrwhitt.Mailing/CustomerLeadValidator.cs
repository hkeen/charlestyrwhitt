﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentValidation;

namespace CharlesTyrwhitt.Mailing
{
    public class CustomerLeadValidator : AbstractValidator<CustomerLead>, ICustomerLeadValidator
    {
        public CustomerLeadValidator()
        {
            RuleFor(x => x.Firstname)
                .NotEmpty();
            RuleFor(x => x.Lastname)
                .NotEmpty();
            RuleFor(x => x.EmailAddress)
                .EmailAddress();
        }

        public bool IsValid(CustomerLead customerLead)
        {
            return this.Validate(customerLead).IsValid;
        }
    }
}
