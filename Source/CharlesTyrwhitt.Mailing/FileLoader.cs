﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace CharlesTyrwhitt.Mailing
{
    public class FileLoader : IFileLoader
    {
        public Stream LoadFile(string path)
        {
            return File.OpenRead(path);
        }
    }
}
