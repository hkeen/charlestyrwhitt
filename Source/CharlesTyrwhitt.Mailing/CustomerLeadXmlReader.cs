﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Text;
using System.IO;
using System.Xml;

namespace CharlesTyrwhitt.Mailing
{
    public class CustomerLeadXmlReader : ICustomerLeadReader
    {
        ICustomerLeadValidator _validator;

        public CustomerLeadXmlReader(ICustomerLeadValidator validator)
        {
            _validator = validator;
        }

        public IEnumerable<CustomerLead> Read(Stream stream)
        {
            List<CustomerLead> customerLeads = new List<CustomerLead>();

            try
            {
                var document = XDocument.Load(stream);
                var xCustomerLeads = document.Element("CustomerLeads")
                                        .Elements("CustomerLead");

                foreach (var xElement in xCustomerLeads)
                {
                    var lead = ParseCustomerLead(xElement);
                    
                    if(_validator.IsValid(lead))
                        customerLeads.Add(lead);
                }
            }
            catch (XmlException) {}
            
            return customerLeads;
        }

        CustomerLead ParseCustomerLead(XElement element)
        {
            string firstname = element.Element("FirstName").Value;
            string lastname = element.Element("LastName").Value;
            string email = element.Element("Email").Value;

            return new CustomerLead
                            {
                                Firstname = firstname,
                                Lastname = lastname,
                                EmailAddress = email
                            };
        }
    }
}
