﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CharlesTyrwhitt.Mailing
{
    public interface ICustomerLeadValidator
    {
        bool IsValid(CustomerLead customerLead);
    }
}
